package musaj.hasan.revoultconverter.framework.api.mapper

import musaj.hasan.domain.entity.RatesEntity
import musaj.hasan.revoultconverter.framework.models.response.RatesResult
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class RatesMapper @Inject constructor(
) :EntityMapper<RatesResult, RatesEntity>{

    override fun fromRemote(remote: RatesResult): RatesEntity {
        return RatesEntity(
            remote.base,
            LocalDate.parse(remote.date, DateTimeFormatter.ISO_DATE),
            remote.rates
            )
    }

    override fun toRemote(entity: RatesEntity): RatesResult {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}