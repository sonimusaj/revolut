package musaj.hasan.revoultconverter.framework.api.mapper

interface EntityMapper<Remote: Any, Entity: Any> {

    open fun fromRemote(remote: Remote): Entity

    open fun toRemote(entity: Entity): Remote
}