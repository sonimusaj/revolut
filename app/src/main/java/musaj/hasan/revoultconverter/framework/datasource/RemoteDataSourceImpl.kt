package musaj.hasan.revoultconverter.framework.datasource

import io.reactivex.Observable
import musaj.hasan.data.datasource.RemoteDataSource
import musaj.hasan.domain.entity.RatesEntity
import musaj.hasan.revoultconverter.framework.api.RatesAPI
import musaj.hasan.revoultconverter.framework.api.mapper.RatesMapper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteDataSourceImpl @Inject constructor(
    private val ratesAPI: RatesAPI,
    private val ratesMapper: RatesMapper
): RemoteDataSource {

    override fun getRates(baseCurrency: String) : Observable<RatesEntity>{
        return ratesAPI.getRates(baseCurrency)
            .map {
                ratesMapper.fromRemote(it)
            }
    }

}