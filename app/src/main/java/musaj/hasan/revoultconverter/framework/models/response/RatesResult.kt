package musaj.hasan.revoultconverter.framework.models.response

import com.google.gson.annotations.SerializedName

data class RatesResult(
    @SerializedName("base")
    val base: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("rates")
    val rates: HashMap<String, Double>
)