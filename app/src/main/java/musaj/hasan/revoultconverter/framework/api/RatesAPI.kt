package musaj.hasan.revoultconverter.framework.api

import retrofit2.http.GET
import io.reactivex.Observable
import musaj.hasan.revoultconverter.framework.models.response.RatesResult
import retrofit2.http.Query

interface RatesAPI{

    @GET("/latest")
    fun getRates(
        @Query("base") baseCurrency: String
    ): Observable<RatesResult>

}