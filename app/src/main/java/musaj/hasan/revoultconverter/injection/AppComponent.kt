package musaj.hasan.revoultconverter.injection

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        FrameworkModule::class,
        DataModule::class,
        ViewModelModule::class,
        AppModule::class,
        ActivityBindingModule::class,
        FragmentBindingModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {
    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(app: Application): Builder

        fun frameworkModule(module: FrameworkModule): Builder

        fun dataModule(module: DataModule): Builder

        fun build(): AppComponent
    }
}