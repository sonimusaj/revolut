package musaj.hasan.revoultconverter.injection

import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import androidx.lifecycle.ViewModel
import musaj.hasan.revoultconverter.ui.viewmodels.RatesViewModel

@Module
class ViewModelModule{

    @Provides
    @IntoMap
    @ViewModelKey(RatesViewModel::class)
    fun provideRatesViewModel(viewModel: RatesViewModel): ViewModel = viewModel

}