package musaj.hasan.revoultconverter.injection

import dagger.Module
import dagger.android.ContributesAndroidInjector
import musaj.hasan.revoultconverter.ui.views.MainActivity

@Module
abstract class ActivityBindingModule{
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity
}