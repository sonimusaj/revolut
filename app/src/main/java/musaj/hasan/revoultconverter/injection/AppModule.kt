package musaj.hasan.revoultconverter.injection

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import musaj.hasan.domain.AndroidSchedulerProvider
import musaj.hasan.domain.SchedulerProvider
import javax.inject.Singleton

@Module
open class AppModule{
    @Provides
    @Singleton
    open fun provideApplicationContext(app: Application): Context = app

    @Provides
    @Singleton
    open fun provideSchedulerProvider(): SchedulerProvider = AndroidSchedulerProvider()

    @Provides
    @Singleton
    open fun provideViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory = factory
}