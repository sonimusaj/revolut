package musaj.hasan.revoultconverter.injection

import dagger.Module
import dagger.Provides
import musaj.hasan.data.datasource.RemoteDataSource
import musaj.hasan.data.datasource.repository.RatesRepositoryImpl
import musaj.hasan.domain.repository.RatesRepository
import musaj.hasan.revoultconverter.framework.datasource.RemoteDataSourceImpl
import javax.inject.Singleton

@Module
open class DataModule{
    @Provides
    @Singleton
    fun provideRatesRepository(r: RatesRepositoryImpl): RatesRepository = r


    @Provides
    @Singleton
    fun provideRemoteDataSource(ds: RemoteDataSourceImpl): RemoteDataSource = ds



}