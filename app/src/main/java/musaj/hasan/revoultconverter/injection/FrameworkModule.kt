package musaj.hasan.revoultconverter.injection

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import musaj.hasan.domain.SchedulerProvider
import musaj.hasan.revoultconverter.BuildConfig
import musaj.hasan.revoultconverter.framework.api.RatesAPI
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
open class FrameworkModule{

    @Provides
    @Singleton
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient{
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }
        return OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

    }

    @Provides
    @Singleton
    @Named("Rates")
    fun provideRatesRetrofit(schedulerProvider: SchedulerProvider, gson: Gson, okHttpClient: OkHttpClient) =
        createRetrofit(schedulerProvider, gson, okHttpClient, "https://revolut.duckdns.org/")

    @Provides
    @Singleton
    open fun provideRatesAPI(@Named("Rates") retrofit: Retrofit, context: Context): RatesAPI = retrofit.create(RatesAPI::class.java)

    private fun createRetrofit(
        schedulerProvider: SchedulerProvider,
        gson: Gson,
        okHttpClient: OkHttpClient,
        endpoint: String
        ): Retrofit{
        return Retrofit.Builder()
            .baseUrl(endpoint)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(schedulerProvider.io()))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }
}