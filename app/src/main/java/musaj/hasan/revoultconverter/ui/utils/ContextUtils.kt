package musaj.hasan.revoultconverter.ui.utils

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog

fun Context.showPopup(title: String, message: String, buttonText: String, listener:(DialogInterface, Int) -> Unit = {d, i ->}){
   AlertDialog.Builder(this)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(buttonText, listener)
        .create()
        .show()
}