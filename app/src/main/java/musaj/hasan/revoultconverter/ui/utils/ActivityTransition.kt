package musaj.hasan.revoultconverter.ui.utils

import java.io.Serializable

enum class ActivityTransition : Serializable {
    DEFAULT,
    DEFAULT_INVERSE,
    ENTER_FROM_RIGHT,
    EXIT_TO_RIGHT;

    fun inverse(): ActivityTransition {
        return when(this) {
            DEFAULT -> DEFAULT_INVERSE
            DEFAULT_INVERSE -> DEFAULT
            ENTER_FROM_RIGHT -> EXIT_TO_RIGHT
            EXIT_TO_RIGHT -> ENTER_FROM_RIGHT
        }
    }

}