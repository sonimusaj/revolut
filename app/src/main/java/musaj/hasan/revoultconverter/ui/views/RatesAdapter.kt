package musaj.hasan.revoultconverter.ui.views

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.view_rate_item.view.*
import musaj.hasan.revoultconverter.R
import musaj.hasan.revoultconverter.ui.uimodels.RateUiModel
import java.lang.Exception
import java.math.RoundingMode
import javax.inject.Inject

open class RatesAdapter @Inject constructor(): RecyclerView.Adapter<RatesAdapter.RatesViewHolder>(){

    var cursorPosition = 0
    var data = mutableListOf<RateUiModel>()
    set(value){
        field = value
        notifyDataSetChanged()
    }

    var onItemClickListener: (RateUiModel) -> Unit = {}
    var onValueChangeListener: (RateUiModel) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.view_rate_item, parent, false)
        return RatesViewHolder(v)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: RatesViewHolder, position: Int) {
        return holder.render(data[position])
    }

    inner class RatesViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView){
        fun render(entry: RateUiModel) = with(itemView) {
            rate_iso3.text = entry.iso3Name
            rate_full_name.text = entry.fullName
            rate_flag.setImageResource(entry.flagResouce)

            val convertedValue = if (adapterPosition != 0) entry.value * data[0].value else entry.value
            val roundedValue = convertedValue.toBigDecimal().setScale(4, RoundingMode.UP).toDouble()
            rate_value.setText(roundedValue.toString())

            rate_value.isEnabled = adapterPosition == 0

            if (adapterPosition == 0 && cursorPosition <= rate_value.text.toString().length)
                rate_value.setSelection(cursorPosition)

            this.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    notifyItemMoved(adapterPosition, 0)
                    data.removeAt(adapterPosition)
                        .also {
                            data.add(0, it)
                        }
                    entry.value = rate_value.text.toString().toDouble()
                    onItemClickListener.invoke(entry)
                }
            }

            if (adapterPosition == 0) {
                rate_value.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {
                        val entity = data[0]
                        entity.value = p0?.toString()?.toDouble()?:0.0
                        onValueChangeListener.invoke(entity)
                        cursorPosition = rate_value.selectionEnd
                        try{
                            notifyDataSetChanged()
                        }catch (e: Exception){

                        }
                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }
                })

                //handle cursor unexpected position change on notifyDataSetChange
                rate_value.selectionChangeListener = {
                    cursorPosition = rate_value.selectionEnd
                }
            }
        }
    }
}