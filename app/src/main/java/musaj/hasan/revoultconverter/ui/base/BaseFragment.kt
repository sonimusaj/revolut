package musaj.hasan.revoultconverter.ui.base

import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerFragment
import musaj.hasan.revoultconverter.ui.Navigator
import musaj.hasan.revoultconverter.ui.utils.Loader
import javax.inject.Inject

open class BaseFragment : DaggerFragment(){
    @Inject
    lateinit var navigator: Navigator
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var loader: LoaderDialog? = null

    fun bindStandardLoader(loaderState: Loader?) {
        if (loaderState != null) {
            if (loaderState == Loader.SHOW) {
                if (loader != null) {
                    loader!!.dismiss()
                }

                loader = LoaderDialog()
                loader!!.show(childFragmentManager.beginTransaction(), LoaderDialog.TAG)
            } else {
                if (loader != null ) {
                    loader!!.dismiss()
                }
            }

        }
    }

    override fun onPause() {
        bindStandardLoader(Loader.HIDE)
        super.onPause()
    }

    fun showLoader() {
        bindStandardLoader(Loader.SHOW)
    }

    fun hideLoader() {
        bindStandardLoader(Loader.HIDE)
    }
}