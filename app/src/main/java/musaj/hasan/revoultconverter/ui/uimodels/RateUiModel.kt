package musaj.hasan.revoultconverter.ui.uimodels

data class RateUiModel(
    val iso3Name: String,
    val fullName: String,
    var value: Double,
    val flagResouce: Int
)