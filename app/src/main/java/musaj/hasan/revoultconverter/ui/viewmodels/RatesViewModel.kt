package musaj.hasan.revoultconverter.ui.viewmodels

import android.app.Application
import android.util.Log
import com.blongho.country_data.World
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import musaj.hasan.domain.entity.RatesEntity
import musaj.hasan.domain.usecase.GetRatesUseCase
import musaj.hasan.revoultconverter.R
import musaj.hasan.revoultconverter.ui.base.BaseViewModel
import musaj.hasan.revoultconverter.ui.uimodels.RateUiModel
import musaj.hasan.revoultconverter.ui.utils.Resource
import java.util.concurrent.TimeUnit
import javax.inject.Inject

open class RatesViewModel @Inject constructor(
    application: Application,
    val ratesUseCase: GetRatesUseCase
): BaseViewModel(application){

    val ratesResource = Resource<MutableList<RateUiModel>>()

    var lastRate = RateUiModel("EUR", "Euro", 1.0,  R.drawable.pt)

    fun getRatesRepeatedly(baseRate: RateUiModel? = null) {
        if(baseRate != null)
            lastRate = baseRate
        val delay = 1L
        addDisposable(
            Observable.timer(delay, TimeUnit.SECONDS)
                .repeat()
                .flatMap {
                    ratesUseCase.execute(GetRatesUseCase.Params(lastRate.iso3Name))
                        .flatMap {
                            val currencies = World.getAllCurrencies()
                            val data =  mutableListOf<RateUiModel>()
                            it.rates.forEach{map ->
                               val index = currencies.indexOfFirst {currency ->
                                    currency.code ==  map.key
                                }

                                val currency = currencies[index]
                                val entity = RateUiModel(
                                    currency.code,
                                    currency.name,
                                    map.value,
                                    World.getCountryFrom(currency.country.toLowerCase()).flagResource
                                )
                                data.add(entity)
                            }
                            data.sortBy { it.iso3Name }

                            //add selected currency
                            data.add(0, lastRate)

                            Observable.just(data)
                        }
                },
            ratesResource
        )
    }

}