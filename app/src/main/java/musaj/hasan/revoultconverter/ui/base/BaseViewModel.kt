package musaj.hasan.revoultconverter.ui.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import musaj.hasan.revoultconverter.ui.utils.Loader
import musaj.hasan.revoultconverter.ui.utils.Resource

open class BaseViewModel(app: Application): AndroidViewModel(app){

    open val mHashCompositeDisposable: HashMap<String, CompositeDisposable> = hashMapOf()

    fun <Output> addDisposable(
        observable: Observable<Output>,
        resource: Resource<Output>?=null
    ) {

        var key=observable.hashCode().toString()
        if(resource!=null) {
            key=resource.hashCode().toString()
        }
        if(mHashCompositeDisposable.containsKey(key)){
            mHashCompositeDisposable[key]?.clear()
        }
        if(mHashCompositeDisposable[key]==null){
            mHashCompositeDisposable[key]= CompositeDisposable()
        }

        mHashCompositeDisposable[key]!!.add(observable.subscribeResource(resource))
    }

    private fun <Output> Observable<Output>.subscribeResource(resource: Resource<Output>? = null) =
        doOnSubscribe { resource?.postLoading(Loader.SHOW) }
            .doOnNext { resource?.postLoading(Loader.HIDE) }
            .doOnTerminate { resource?.postLoading(Loader.HIDE) }
            .subscribe(
                { model ->
                    resource?.postFailure(null)
                    resource?.postSuccess(model)
                },
                { err ->
                    err.printStackTrace()
                    resource?.postFailure(err)
                }
            )

    override fun onCleared() {
        for(entity in mHashCompositeDisposable){
            entity.value.dispose()
        }
        super.onCleared()
    }
}