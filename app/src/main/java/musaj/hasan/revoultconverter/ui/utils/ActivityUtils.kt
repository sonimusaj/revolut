package musaj.hasan.revoultconverter.ui.utils

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import musaj.hasan.revoultconverter.R


inline fun <reified T : ViewModel> FragmentActivity.getViewModel(viewModelFactory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}

fun <Model> Fragment.observe(
    resource: Resource<Model>,
    success: (Model) -> Unit = {},
    failure: (Throwable) -> Unit = {},
    loading: (Loader) -> Unit = {}
){
    resource.successUpdates().observe(this.viewLifecycleOwner, Observer { r -> success.invoke(r)})
    resource.failureUpdates().observe(this.viewLifecycleOwner, Observer{ r -> r?.let(failure)})
    resource.loadingUpdates().observe(this.viewLifecycleOwner, Observer{ r -> r?.let(loading)})
}

fun <Model> AppCompatActivity.observe(
    resource: Resource<Model>,
    success: (Model) -> Unit = {},
    failure: (Throwable) -> Unit = {},
    loading: (Loader) -> Unit = {}
) {
    resource.successUpdates().observe(this, Observer { r -> success.invoke(r) })
    resource.failureUpdates().observe(this, Observer { r -> r?.let(failure) })
    resource.loadingUpdates().observe(this, Observer { r -> r?.let(loading) })
}

fun Activity.enterFromRight(){
    this.overridePendingTransition(
        R.anim.slide_from_right,
        R.anim.slide_to_left
    )
}

fun Activity.exitToRight() {
    this.overridePendingTransition(
        R.anim.slide_from_left,
        R.anim.slide_to_right
    )
}

fun Activity.startActivityWithTransition(intent: Intent, transition: ActivityTransition = ActivityTransition.DEFAULT, requestCode: Int? = null){
    intent.putExtra("ENTRY_TRANSITION", transition)
    if(requestCode != null) {
        this.startActivityForResult(intent, requestCode)
    }else{
        this.startActivity(intent)
    }
    this.applyTransition(transition)
}

fun Activity.applyTransition(transition: ActivityTransition = ActivityTransition.DEFAULT) {
    when(transition) {
        ActivityTransition.DEFAULT -> { }
        ActivityTransition.ENTER_FROM_RIGHT -> this.enterFromRight()
        ActivityTransition.EXIT_TO_RIGHT -> this.exitToRight()
    }
}