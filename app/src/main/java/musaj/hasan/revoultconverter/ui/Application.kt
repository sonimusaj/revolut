package musaj.hasan.revoultconverter.ui

import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import musaj.hasan.revoultconverter.injection.DaggerAppComponent

open class Application: DaggerApplication(){

    var applicationInjector: AndroidInjector<DaggerApplication>? = null

    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        if(applicationInjector == null){
            applicationInjector = provideApplicationInjector()
        }

        return applicationInjector as AndroidInjector<DaggerApplication>
    }

    open fun provideApplicationInjector(): AndroidInjector<DaggerApplication>? {
        return DaggerAppComponent.builder()
            .application(this)
            .build()
    }

}