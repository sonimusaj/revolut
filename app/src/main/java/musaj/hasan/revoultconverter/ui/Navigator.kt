package musaj.hasan.revoultconverter.ui

import android.app.Activity
import musaj.hasan.revoultconverter.ui.utils.ActivityTransition
import musaj.hasan.revoultconverter.ui.utils.startActivityWithTransition
import musaj.hasan.revoultconverter.ui.views.MainActivity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class Navigator @Inject constructor(){

    fun launchMainActivity(activity: Activity){
        activity.startActivityWithTransition(MainActivity.getCallingIntent(activity), ActivityTransition.ENTER_FROM_RIGHT)
    }
}