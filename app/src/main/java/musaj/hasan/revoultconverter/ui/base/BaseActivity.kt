package musaj.hasan.revoultconverter.ui.base

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import musaj.hasan.revoultconverter.ui.Navigator
import musaj.hasan.revoultconverter.ui.utils.Loader
import javax.inject.Inject

open class BaseActivity: DaggerAppCompatActivity(){

    var loader: LoaderDialog? = null
    @Inject
    lateinit var navigator: Navigator
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    fun bindStandardLoader(loaderState: Loader?) {
        if (loaderState != null) {
            if (loaderState == Loader.SHOW) {
                if (loader != null) {
                    loader!!.dismiss()
                }

                loader = LoaderDialog()
                loader!!.show(supportFragmentManager!!.beginTransaction(),
                    LoaderDialog.TAG
                )
            } else {
                if (loader != null) {
                    loader!!.dismiss()
                }
            }

        }
    }

    fun showLoader() {
        bindStandardLoader(Loader.SHOW)
    }

    fun hideLoader() {
        bindStandardLoader(Loader.HIDE)
    }

    override fun onPause() {
        hideLoader()
        super.onPause()
    }
}