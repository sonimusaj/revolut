package musaj.hasan.revoultconverter.ui.utils

import androidx.lifecycle.MutableLiveData

open class Resource<Output>(
    protected open val success: MutableLiveData<Output> = MutableLiveData(),
    protected open val failure: MutableLiveData<Throwable?> = MutableLiveData(),
    protected open val loading: MutableLiveData<Loader> = MutableLiveData()
){
    fun successUpdates() = success
    fun failureUpdates() = failure
    fun loadingUpdates() = loading

    open fun postSuccess(data: Output){
        success.postValue(data)
    }

    open fun postLoading(loader: Loader){
        loading.postValue(loader)
    }

    open fun postFailure(t: Throwable?){
        failure.postValue(t)
    }
}