package musaj.hasan.revoultconverter.ui.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.blongho.country_data.Currency
import com.blongho.country_data.World
import kotlinx.android.synthetic.main.activity_main.*
import musaj.hasan.revoultconverter.R
import musaj.hasan.revoultconverter.ui.base.BaseActivity
import musaj.hasan.revoultconverter.ui.utils.getViewModel
import musaj.hasan.revoultconverter.ui.utils.observe
import musaj.hasan.revoultconverter.ui.utils.showPopup
import musaj.hasan.revoultconverter.ui.viewmodels.RatesViewModel
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var ratesAdapter: RatesAdapter

    lateinit var ratesViewModel: RatesViewModel
    private lateinit var currencies: List<Currency>

    companion object{
        fun getCallingIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ratesViewModel = getViewModel(viewModelFactory)

        World.init(applicationContext)

        currencies = World.getAllCurrencies()

        rates_rv.adapter = ratesAdapter

        ratesAdapter.onItemClickListener = {entity ->
            rates_rv.scrollToPosition(0)
            ratesViewModel.getRatesRepeatedly(entity)
        }

        ratesAdapter.onValueChangeListener = {entity ->

        }

        with(ratesViewModel){
            observe(ratesResource,
                success = {
                    ratesAdapter.data = it
                },
                failure = {
                    showPopup("", getString(R.string.error), getString(R.string.retry)){d, i ->
                        ratesViewModel.getRatesRepeatedly()
                    }

                },
                loading = {
                    Log.d("Response", "Loading $it")
                })

            getRatesRepeatedly()
        }
    }
}
