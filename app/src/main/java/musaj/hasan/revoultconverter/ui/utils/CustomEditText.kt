package musaj.hasan.revoultconverter.ui.utils

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.widget.EditText
import androidx.annotation.RequiresApi

open class CustomEditText: EditText {

    var selectionChangeListener:() -> Unit = {}

    @JvmOverloads
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        super.onSelectionChanged(selStart, selEnd)
        if(selectionChangeListener != null){
            selectionChangeListener?.invoke()
        }
    }

}