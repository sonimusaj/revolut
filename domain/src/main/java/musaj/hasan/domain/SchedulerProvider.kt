package musaj.hasan.domain

interface SchedulerProvider {


    @io.reactivex.annotations.NonNull abstract fun computation(): io.reactivex.Scheduler

    @io.reactivex.annotations.NonNull abstract fun io(): io.reactivex.Scheduler

    @io.reactivex.annotations.NonNull abstract fun ui(): io.reactivex.Scheduler
}
