package musaj.hasan.domain.repository

import io.reactivex.Observable
import musaj.hasan.domain.entity.RatesEntity

interface RatesRepository {

    fun getRates(baseCurrency: String): Observable<RatesEntity>

}