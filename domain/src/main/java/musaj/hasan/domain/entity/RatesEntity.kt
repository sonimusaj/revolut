package musaj.hasan.domain.entity

import org.threeten.bp.LocalDate

data class RatesEntity(
    val base: String,
    val date: LocalDate,
    val rates: HashMap<String, Double>
)