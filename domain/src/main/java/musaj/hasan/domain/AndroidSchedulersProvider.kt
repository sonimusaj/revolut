package musaj.hasan.domain

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers


class AndroidSchedulerProvider : SchedulerProvider {
    override fun computation(): Scheduler {
        return Schedulers.computation()
    }

    override fun io(): Scheduler {
        return Schedulers.io()
    }

    override fun ui(): Scheduler {
        return Schedulers.io()//todo set to mainthread
    }
}
