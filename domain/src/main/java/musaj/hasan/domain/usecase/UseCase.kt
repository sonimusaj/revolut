package musaj.hasan.domain.usecase

import io.reactivex.Observable
import musaj.hasan.domain.SchedulerProvider

abstract class UseCase<Input, Output> constructor(
    private val schedulerProvider: SchedulerProvider){

    internal abstract fun buildObservable(params: Input): Observable<Output>

    open fun execute(params: Input): Observable<Output>{
        return buildObservable(params)
            .subscribeOn(schedulerProvider.computation())
            .observeOn(schedulerProvider.ui())
    }

    class Params()

}