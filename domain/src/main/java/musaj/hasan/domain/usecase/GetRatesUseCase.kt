package musaj.hasan.domain.usecase

import io.reactivex.Observable
import musaj.hasan.domain.SchedulerProvider
import musaj.hasan.domain.entity.RatesEntity
import musaj.hasan.domain.repository.RatesRepository
import java.util.*
import javax.inject.Inject

open class GetRatesUseCase @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val ratesRepository: RatesRepository
): UseCase<GetRatesUseCase.Params, RatesEntity>(schedulerProvider){

    override fun buildObservable(params: Params): Observable<RatesEntity> {
        return ratesRepository.getRates(params.baseCurrency)
    }

    data class Params(val baseCurrency: String)

}