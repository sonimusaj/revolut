package musaj.hasan.data.datasource.repository

import io.reactivex.Observable
import musaj.hasan.data.datasource.RemoteDataSource
import musaj.hasan.domain.entity.RatesEntity
import musaj.hasan.domain.repository.RatesRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RatesRepositoryImpl @Inject constructor(
        private val remoteDataSource: RemoteDataSource
): RatesRepository{
    override fun getRates(baseCurrency: String): Observable<RatesEntity> {
        return remoteDataSource.getRates(baseCurrency)
    }
}