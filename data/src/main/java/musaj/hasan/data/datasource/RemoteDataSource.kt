package musaj.hasan.data.datasource

import io.reactivex.Observable
import musaj.hasan.domain.entity.RatesEntity

interface RemoteDataSource {
    fun getRates(baseCurrency: String): Observable<RatesEntity>
}